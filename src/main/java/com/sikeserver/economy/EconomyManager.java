package com.sikeserver.economy;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

import org.bukkit.plugin.java.JavaPlugin;

import com.sikeserver.core.CoreManager;
import com.sikeserver.economy.commands.CommandMoney;

public class EconomyManager extends JavaPlugin {
	public CoreManager core;
	private static EconomyManager plugin;

	public static final String CMD_ROOT = "money";

	/**
	 * プラグインが有効化されたときに呼び出されるイベント
	 * 
	 * @author Siketyan
	 */
	@Override
	public void onEnable() {
		// CoreプラグインのCoreManagerクラスを取得
		core = CoreManager.getPlugin();
		// pluginにこのクラスを代入
		plugin = this;

		getCommand(CMD_ROOT).setExecutor(new CommandMoney());
	}

	/**
	 * 指定されたUUIDのプレイヤーの所持金を返します。
	 * 
	 * @param uuid
	 *            プレイヤーのUUID（ハイフン付き）
	 * 
	 * @return 所持金。プレイヤーが登録されていないときは-1を返します。
	 */
	public long getMoney(UUID uuid) throws SQLException {
		try (Statement stmt = core.sql.getStatement();
				ResultSet result = stmt.executeQuery(
						"SELECT * FROM " + CoreManager.playerTable + " WHERE uuid = \"" + uuid.toString() + "\"")) {
			if (result.next())// プレイヤーが存在するなら
			{
				long money = result.getLong("money");
				return money;
			} else
				return -1;
		}
	}

	public long getMoney(String name) throws SQLException {
		try (Statement stmt = core.sql.getStatement();
				ResultSet result = stmt
						.executeQuery("SELECT * FROM " + CoreManager.playerTable + " WHERE mcid = \"" + name + "\"")) {
			if (result.next())// プレイヤーが存在するなら
			{
				long money = result.getLong("money");
				return money;
			} else
				return -1;
		}
	}

	/**
	 * 指定されたUUIDのプレイヤーの所持金を変更します。
	 * 
	 * @author Siketyan
	 * 
	 * @param uuid
	 *            プレイヤーのUUID<b>（ハイフン付き）</b>
	 * @param money
	 *            変更後の所持金
	 * 
	 * @return クエリの戻り行数を返します。
	 * 
	 * @throws SQLException
	 */
	public int setMoney(UUID uuid, long money) throws SQLException {
		return core.sql.executeUpdate("UPDATE " + CoreManager.playerTable + " SET money = " + money + " WHERE uuid = \""
				+ uuid.toString() + "\"");
	}

	/**
	 * 指定された名前のプレイヤーの所持金を変更します。
	 * 
	 * @author Siketyan
	 * 
	 * @param uuid
	 *            プレイヤーのMCID
	 * @param money
	 *            変更後の所持金
	 * 
	 * @return クエリの戻り行数を返します。
	 * 
	 * @throws SQLException
	 */
	public int setMoney(String name, long money) throws SQLException {
		return core.sql.executeUpdate(
				"UPDATE " + CoreManager.playerTable + " SET money = " + money + " WHERE mcid = \"" + name + "\"");
	}

	/**
	 * EconomyManagerを取得します。
	 * 
	 * @author Siketyan
	 * 
	 * @return EconomyManagerぽいぽい
	 */
	public static EconomyManager getPlugin() {
		return plugin;
	}
}

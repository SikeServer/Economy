package com.sikeserver.economy.commands;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Logger;

import org.apache.commons.lang.math.NumberUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sikeserver.core.CoreManager;
import com.sikeserver.economy.EconomyManager;

public class CommandMoney implements CommandExecutor {
	private EconomyManager plugin;
	private CoreManager core;
	private Logger logger;

	/**
	 * このクラスのコンストラクタ。 インスタンスが生成されたときに、各動的クラス/変数を取得して代入します。
	 * 
	 * @author Siketyan
	 */
	public CommandMoney() {
		plugin = EconomyManager.getPlugin();
		core = plugin.core;
		logger = plugin.getLogger();
	}

	/**
	 * moneyコマンドが実行されたときのイベント。 メインクラスでExecutor登録する必要があります。
	 * 
	 * @author Siketyan
	 */
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) // プレイヤー以外から実行されたら
		{
			// 各コマンドを別関数で実行（コンソールからは実行できません）
			if (args.length < 1)
				return displayMine(sender);
			if (args[0].equalsIgnoreCase("pay"))
				return pay(sender, args);
		} else {
			if (args.length < 1) {
				sender.sendMessage(core.getMessage("Error.Common.Console"));
				return true;
			}
			if (args[0].equalsIgnoreCase("pay")) {
				sender.sendMessage(core.getMessage("Error.Common.Console"));
				return true;
			}
		}

		// 各コマンドを別関数で実行（コンソールからも実行できます）
		if (args[0].equalsIgnoreCase("give"))
			return give(sender, args);
		if (args[0].equalsIgnoreCase("take"))
			return take(sender, args);
		if (args[0].equalsIgnoreCase("set"))
			return set(sender, args);
		if (args[0].equalsIgnoreCase("help"))
			return help(sender);
		if (args[0].equalsIgnoreCase("top"))
			return top(sender);
		if (args.length == 1)
			return displayOthers(sender, args);

		// 何も引っかからなかったら使い方を表示
		return false;
	}

	/**
	 * 自分の所持金をsenderへ送信します。 <b>コンソールから実行できないことに注意してください。</b>
	 * 
	 * @author Siketyan
	 * 
	 * @param sender
	 *            onCommandの引数にあるものをそのまま
	 * 
	 * @return 実行完了したか。falseでUsageが表示される。
	 */
	private boolean displayMine(CommandSender sender) {
		Player p = (Player) sender;
		try {
			long balance = plugin.getMoney(p.getUniqueId());
			sender.sendMessage(core.getMessage("Message.Money.Mine").replaceAll("%balance%", Long.toString(balance)));
			return true;
		} catch (SQLException e) {
			return errorSQL(sender, e);
		}
	}

	/**
	 * 他のプレイヤーの所持金を確認します。 権限<i>SikeServer.Economy.Others</i>が必要です。
	 * 
	 * @author Siketyan
	 * 
	 * @param sender
	 *            onCommandの引数にあるものをそのまま
	 * @param args
	 *            同上
	 * 
	 * @return 実行完了したか。falseでUsageが表示される。
	 */
	private boolean displayOthers(CommandSender sender, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			if (!p.hasPermission("SikeServer.Economy.Others"))
				return errorPerm(sender);
		}

		try {
			long balance = plugin.getMoney(args[0]);
			if (balance == -1) {
				sender.sendMessage(core.getMessage("Error.Common.UnknownPlayer"));
				return true;
			} else {
				sender.sendMessage(
						core.getMessage("Message.Money.Others").replaceAll("%player%", core.getNameWithCase(args[0]))
								.replaceAll("%balance%", Long.toString(balance)));
				return true;
			}
		} catch (SQLException e) {
			return errorSQL(sender, e);
		}
	}

	/**
	 * 他のプレイヤーの所持金を指定された額だけ増やします。 権限<i>SikeServer.Economy.Give</i>が必要です。
	 * 
	 * @author Siketyan
	 * 
	 * @param sender
	 *            onCommandの引数にあるものをそのまま
	 * @param args
	 *            同上
	 * 
	 * @return 実行完了したか。falseでUsageが表示される。
	 */
	private boolean give(CommandSender sender, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			if (!p.hasPermission("SikeServer.Economy.Give"))
				return errorPerm(sender);
		}

		if (args.length != 3) {
			sender.sendMessage(core.getMessage("Error.Common.Args"));
			return false;
		}
		if (!NumberUtils.isNumber(args[2])) {
			sender.sendMessage(core.getMessage("Error.Economy.FormatAmount"));
			return true;
		}

		long current;
		long amount = Long.parseLong(args[2]);
		if (amount < 0) {
			sender.sendMessage(core.getMessage("Error.Economy.MinusAmount"));
			return true;
		}

		try {
			current = plugin.getMoney(args[1]);

			if (current != -1) {
				plugin.setMoney(args[1], current + amount);
				sender.sendMessage(core.getMessage("Message.Money.Gave")
						.replaceAll("%player%", core.getNameWithCase(args[1])).replaceAll("%amount%", args[2]));
				return true;
			} else {
				sender.sendMessage(core.getMessage("Error.Common.UnknownPlayer"));
				return true;
			}
		} catch (SQLException e) {
			return errorSQL(sender, e);
		}
	}

	/**
	 * 他のプレイヤーの所持金を指定された額だけ減らします。 権限<i>SikeServer.Economy.Took</i>が必要です。
	 * 
	 * @author Siketyan
	 * 
	 * @param sender
	 *            onCommandの引数にあるものをそのまま
	 * @param args
	 *            同上
	 * 
	 * @return 実行完了したか。falseでUsageが表示される。
	 */
	private boolean take(CommandSender sender, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			if (!p.hasPermission("SikeServer.Economy.Take"))
				return errorPerm(sender);
		}

		if (args.length != 3) {
			sender.sendMessage(core.getMessage("Error.Common.Args"));
			return false;
		}
		if (!NumberUtils.isNumber(args[2])) {
			sender.sendMessage(core.getMessage("Error.Economy.FormatAmount"));
			return true;
		}

		long current;
		long amount = Long.parseLong(args[2]);
		if (amount < 0) {
			sender.sendMessage(core.getMessage("Error.Economy.MinusAmount"));
			return true;
		}

		try {
			current = plugin.getMoney(args[1]);
			long balance = (current - amount < 0) ? 0 : current - amount;

			if (current != -1) {
				plugin.setMoney(args[1], balance);
				sender.sendMessage(core.getMessage("Message.Money.Took")
						.replaceAll("%player%", core.getNameWithCase(args[1])).replaceAll("%amount%", args[2]));
				return true;
			} else {
				sender.sendMessage(core.getMessage("Error.Common.UnknownPlayer"));
				return true;
			}
		} catch (SQLException e) {
			return errorSQL(sender, e);
		}
	}

	/**
	 * 他のプレイヤーの所持金を指定された額に設定します。 権限<i>SikeServer.Economy.Set</i>が必要です。
	 * 
	 * @author Siketyan
	 * 
	 * @param sender
	 *            onCommandの引数にあるものをそのまま
	 * @param args
	 *            同上
	 * 
	 * @return 実行完了したか。falseでUsageが表示される。
	 */
	private boolean set(CommandSender sender, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			if (!p.hasPermission("SikeServer.Economy.Set"))
				return errorPerm(sender);
		}

		if (args.length != 3) {
			sender.sendMessage(core.getMessage("Error.Common.Args"));
			return false;
		}
		if (!NumberUtils.isNumber(args[2])) {
			sender.sendMessage(core.getMessage("Error.Economy.FormatAmount"));
			return true;
		}

		long amount = Long.parseLong(args[2]);
		if (amount < 0) {
			sender.sendMessage(core.getMessage("Error.Economy.MinusAmount"));
			return true;
		}

		try {
			if (plugin.getMoney(args[1]) != -1) {
				plugin.setMoney(args[1], amount);
				sender.sendMessage(core.getMessage("Message.Money.Setted")
						.replaceAll("%player%", core.getNameWithCase(args[1])).replaceAll("%balance%", args[2]));
				return true;
			} else {
				sender.sendMessage(core.getMessage("Error.Common.UnknownPlayer"));
				return false;
			}
		} catch (SQLException e) {
			return errorSQL(sender, e);
		}
	}

	/**
	 * 他のプレイヤーへ自分の所持金から指定した額を送金します。
	 * 
	 * @author Siketyan
	 * 
	 * @param sender
	 *            onCommandの引数にあるものをそのまま
	 * @param args
	 *            同上
	 * 
	 * @return 実行完了したか。falseでUsageが表示される。
	 */
	private boolean pay(CommandSender sender, String[] args) {
		Player p = (Player) sender;

		if (args.length != 3) {
			sender.sendMessage(core.getMessage("Error.Common.Args"));
			return false;
		}
		if (!NumberUtils.isNumber(args[2])) {
			sender.sendMessage(core.getMessage("Error.Economy.FormatAmount"));
			return true;
		}

		long amount = Long.parseLong(args[2]);
		if (amount < 0) {
			sender.sendMessage(core.getMessage("Error.Economy.MinusAmount"));
			return true;
		}

		try {
			long current = plugin.getMoney(p.getUniqueId());
			long destMoney = plugin.getMoney(args[1]);

			if (current < amount) {
				sender.sendMessage(core.getMessage("Error.Economy.NotEnoughMoney"));
				return true;
			}

			if (destMoney != -1) {
				plugin.setMoney(p.getUniqueId(), plugin.getMoney(p.getUniqueId()) - amount);
				plugin.setMoney(args[1], destMoney + amount);
				sender.sendMessage(core.getMessage("Message.Money.Paid")
						.replaceAll("%player%", core.getNameWithCase(args[1])).replaceAll("%amount%", args[2]));
				Player dest = Bukkit.getPlayer(args[2]);
				if (dest != null) {
					dest.sendMessage(core.getMessage("Message.Money.Received")
							.replaceAll("%player%", core.getNameWithCase(args[1])).replaceAll("%amount%", args[2]));
				}
				return true;
			} else {
				sender.sendMessage(core.getMessage("Error.Common.UnknownPlayer"));
				return true;
			}
		} catch (SQLException e) {
			return errorSQL(sender, e);
		}
	}

	/**
	 * ヘルプをConfigから取得して送信します。
	 * 
	 * @author Siketyan
	 * 
	 * @param sender
	 *            onCommandの引数にあるものをそのまま
	 * 
	 * @return trueを返します。（Usageを表示しない）
	 */
	private boolean help(CommandSender sender) {
		List<String> helps = core.getMessageList("Message.Money.Help");
		for (String oneLine : helps.toArray(new String[helps.size()])) {
			String msg = ChatColor.translateAlternateColorCodes('&', oneLine);
			sender.sendMessage(msg);
		}
		return true;
	}
	
	private boolean top(CommandSender sender) {
		try (Statement stmt = core.sql.getStatement();
			 ResultSet rs = stmt.executeQuery(
				"SELECT * FROM `core_players` ORDER BY `money` DESC LIMIT 5"
			 ))
		{
			sender.sendMessage(core.getMessage("Message.Money.TopHeader"));
			
			int rank = 1;
			while (rs.next()) {
				sender.sendMessage(
					core.getMessage("Message.Money.TopRank")
						.replaceAll("%rank%", String.valueOf(rank))
						.replaceAll("%player%", rs.getString("mcid"))
						.replaceAll("%amount%", rs.getString("money"))
				);
				rank++;
			}
		} catch (SQLException e) {
			errorSQL(sender, e);
		}
		return true;
	}

	/**
	 * SQLエラーが発生したときの処理を実行します。 めんどくさいのでまとめました。
	 * 
	 * @author Siketyan
	 * 
	 * @param sender
	 *            onCommandの引数そのまま
	 * @param e
	 *            catchの引数そのまま
	 * 
	 * @return trueを返します。（Usageを表示しない）
	 */
	private boolean errorSQL(CommandSender sender, SQLException e) {
		sender.sendMessage(core.getMessage("Error.Common.SQL"));
		logger.warning("Oh,no... Error occurred in SQL query!");
		e.printStackTrace();
		return true;
	}

	/**
	 * 権限不所持の場合の処理を実行します。 これもめんどいのでまとめました。
	 * 
	 * @author Siketyan
	 * 
	 * @param sender
	 *            onCommandの引数そのまま
	 * 
	 * @return trueを返します。（Usageを表示しない）
	 */
	private boolean errorPerm(CommandSender sender) {
		sender.sendMessage(core.getMessage("Error.Common.Permission"));
		return true;
	}
}
